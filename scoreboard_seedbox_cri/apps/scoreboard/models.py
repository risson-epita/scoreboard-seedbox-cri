from django.conf import settings
from django.db import models


class SeedReport(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    time = models.DateTimeField()
    size = models.FloatField()
    image_name = models.CharField(max_length=512)
