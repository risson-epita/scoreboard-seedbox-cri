from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

import re
import datetime

from .models import SeedReport
from .forms import UploadForm


class ScoreboardView(TemplateView):
    template_name = "scoreboard/scoreboard.html"

    def get(self, request, *args, **kwargs):
        context = {}
        scores = []

        for user in User.objects.all():
            # if user.is_superuser:
            #    continue
            seeds = SeedReport.objects.filter(user=user)
            score = 0

            for seed in seeds:
                score += seed.size
            scores.append([user, score])

        scores.sort(key=lambda s: (s[1]), reverse=True)
        context["scores"] = scores

        return render(request, self.template_name, context)


class UploadView(TemplateView):
    template_name = "scoreboard/upload.html"

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("/accounts/login/epita/?next=/upload")
        context = {}
        form = UploadForm()
        context["form"] = form
        return render(request, self.template_name, context)

    def handle_uploaded_file(self, user, file):
        if file.content_type != "text/plain":
            return "File is not of type text/plain."

        content = file.read().decode().split("\n")
        current_image = ""

        for line in content:
            line.strip()

            if "Download complete:" in line:
                current_image = line.split(" ")[-1]

            if "Your share ratio was" not in line:
                continue

            match = re.findall(
                "([0-9][0-9]/[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]).*=([0-9.]+)([KGM]?)iB",  # noqa: E501
                line,
            )

            if match:
                for time, size, unit in match:
                    time = datetime.datetime.strptime(time, "%m/%d %H:%M:%S")
                    time = time.replace(year=2020)
                    size = float(size)
                    if unit == "M":  # If we are using MiB instead of GiB
                        size /= 1024
                    if unit == "K":  # If we are using KiB instead of GiB
                        size /= 1024
                        size /= 1024
                    existing_report = SeedReport.objects.filter(
                        user=user, time=time, size=size
                    )
                    if not existing_report:
                        report = SeedReport.objects.create(
                            user=user, time=time, size=size, image_name=current_image
                        )
                        report.save()

        return None

    # pylint: disable=unused-argument
    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("/accounts/login/epita/?next=/upload")
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = form.cleaned_data["file"]
            error = self.handle_uploaded_file(request.user, file)
            if not error:
                return HttpResponseRedirect("/scoreboard")
            form.add_error("file", error)
        context = {}
        context["form"] = form
        return render(request, self.template_name, context)
